const http = require("http");
const fs = require("fs");
const uuid = require("uuid");

const PORT = process.env.PORT || 5000;

// Creating Server at port 5000
let server = http.createServer((request, response) => {
  if (request.url === "/" && request.method === "GET") {
    response.writeHead(200, { "content-type": "application/json" });
    response.write(JSON.stringify({ Message: "Home Page" }));
    response.end();
  }
  // For Returning html content
  else if (request.url === "/html" && request.method === "GET") {
    fs.readFile("htmlFile.html", "utf8", (err, htmlData) => {
      if (err) {
        response.writeHead(500, { "Content-Type": "application/json" });
        response.write(JSON.stringify({ Error: "Error in reading html data" }));
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(htmlData);
      }
      return response.end();
    });
  } else if (request.url === "/json" && request.method === "GET") {
    //For Returning JSON content
    fs.readFile("jsonFile.json", (err, jsonData) => {
      if (err) {
        response.writeHead(500, { "Content-Type": "application/json" });
        response.write(JSON.stringify({ Error: "Error in reading json data" }));
      } else {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(jsonData);
      }
      return response.end();
    });
  } else if (request.url === "/uuid" && request.method === "GET") {
    //For returning different uuid for each request
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify({ uuid: uuid.v4() }));

    return response.end();
  } else if (
    request.url.startsWith("/status") &&
    request.url.split("/").length === 3 &&
    request.method === "GET"
  ) {
    // For returning status with given code
    let statusCode = request.url.split("/").slice(-1);

    if (http.STATUS_CODES[statusCode] === undefined) {
      response.writeHead(400, { "Content-Type": "application/json" });

      response.write(JSON.stringify({ Error: "Invalid Status Code" }));
    } else {
      response.writeHead(statusCode, { "Content-Type": "application/json" });

      response.write(JSON.stringify({ Status: http.STATUS_CODES[statusCode] }));
    }

    return response.end();
  } else if (
    request.url.startsWith("/delay") &&
    request.url.split("/").length === 3 &&
    request.method === "GET"
  ) {
    // For Delaying responce for given seconds
    let time = Number(request.url.split("/").slice(-1));

    if (isNaN(time) || !(time >= 0 && time <= 10)) {
      response.writeHead(400, { "Content-Type": "application/json" });
      response.write(
        JSON.stringify({
          Error: `Provide a valid delay seconds.Delay seconds must be between 0 and 10`,
        })
      );
      return response.end();
    } else {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(
          JSON.stringify({ Message: `Response delayed by ${time} seconds` })
        );

        return response.end();
      }, time * 1000);
    }
  } else {
    response.writeHead(404, { "Content-Type": "application/json" });
    response.write(JSON.stringify({ Error: `Page Not Found` }));

    return response.end();
  }
});

server.listen(PORT, () => {
  console.log(`Server started at port ${PORT}`);
});
